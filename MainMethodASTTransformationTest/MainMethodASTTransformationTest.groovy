package MainMethodASTTransformationTest

import MainMethodASTTransformation.MainMethodASTTransformation
import org.codehaus.groovy.control.CompilePhase
import org.codehaus.groovy.tools.ast.TransformTestHelper

class MainMethodTransformationTest extends GroovyTestCase{

    public void testInvokeUnitTest() {

        def file = new File('/home/nikl_pod/IdeaProjects/HomeWork3/src/Main/MainMethodExample.groovy')
        assert file.exists()

        def invoker = new TransformTestHelper(new MainMethodASTTransformation(), CompilePhase.CANONICALIZATION)

        def clazz = invoker.parse(file)
        def tester = clazz.newInstance()
        tester.main(null)
    }



    }



