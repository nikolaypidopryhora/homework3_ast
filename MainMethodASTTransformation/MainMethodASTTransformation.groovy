package MainMethodASTTransformation

import org.codehaus.groovy.ast.*
import org.codehaus.groovy.control.CompilePhase
import org.codehaus.groovy.transform.GroovyASTTransformation
import org.codehaus.groovy.transform.ASTTransformation
import org.codehaus.groovy.control.SourceUnit
import org.codehaus.groovy.ast.builder.AstBuilder

@GroovyASTTransformation(phase = CompilePhase.INSTRUCTION_SELECTION)
public class MainMethodASTTransformation implements ASTTransformation {
    void visit(ASTNode[] astNodes, SourceUnit sourceUnit) {
        if (!astNodes) return
        if (!astNodes[0]) return
        if (!astNodes[1]) return
        if (!(astNodes[0] instanceof AnnotationNode)) return
        if (astNodes[0].classNode?.name != MainMethod.class.name) return
        if (!(astNodes[1] instanceof MethodNode)) return

        MethodNode annotatedMethod = astNodes[1]
        ClassNode declaringClass = annotatedMethod.declaringClass
        MethodNode mainMethod = makeMainMethod(annotatedMethod)
        declaringClass.addMethod(mainMethod)
    }

    MethodNode makeMainMethod(MethodNode source) {
        def className = source.declaringClass.name
        def methodName = source.name

        def ast = new AstBuilder().buildFromString(CompilePhase.INSTRUCTION_SELECTION, false, """
            package $source.declaringClass.packageName

            class $source.declaringClass.nameWithoutPackage {
                public static void main(String[] args) {
                    new $className().$methodName()
                }
            }
        """)
        ast[1].methods.find { it.name == 'main' }
    }
}