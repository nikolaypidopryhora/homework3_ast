package MainMethodASTTransformation

/**
 * Created by nikl_pod on 22.11.14.
 */
class MainMethodExample {

        @MainMethod
        public void greet() {
            println "Hello World!"
        }
}
